# READ ME

In this assignment, I am using MySQL as the Database and Node.js as the Backend  
The database and the backend are containerized separetely by using Docker

# Installation

The steps to install and to run are as follow:

```sh
$ git clone https://gitlab.com/rjustin861/assignment1.git
$ cd assignment1
$ docker-compose build
$ docker-compose up -d
```

After running the steps above, you can access the apps by going to: http://localhost:3000

# Populate DB

When the application is run, it will populate tables with the following data:

#### Users table:

| email         | password  |
| ------------- | --------- |
| hello@xyz.com | password1 |
| test@xyz.com  | password2 |

For security, please take note that password is stored as **hash by using bcrypt** in the Database

#### CountryDetails table:

| countryName | gmtOffset |
| ----------- | --------- |
| Indonesia   | GMT+7     |
| Malaysia    | GMT+8     |
| Singapore   | GMT+8     |

# List of APIs

**1. POST /login**  
Usage: This API is to perform login to the apps  
Sample Request:

```sh
 {
     "email": "hello@xyz.com",
     "password": "password1"
 }
```

Sample Response:

```sh
 {
     "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJoZWxsb0B4eXouY29tIiwiaWF0IjoxNTk2MjkxNzc3LCJleHAiOjE1OTYyOTIwNzd9.SlVpeMREQzUs0U-nqY4u86BVQU3JDvcTUjaZP4V_KFY",
     "refreshToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJoZWxsb0B4eXouY29tIiwiaWF0IjoxNTk2MjkxNzc3LCJleHAiOjE1OTYzNzgxNzd9.sdHawKqRx3XmbNizSzsOZb2xwqIXckTxBenQ6CjxfZY"
 }
```

**2. GET /country**  
Usage: This API is to get all country details. For this request, user needs to be authenticated first before he can call this API. To call this API, please pass in token that was obtained from POST /login in the request header _x-auth_  
Sample Request:

```sh
 In the request header:
 x-auth: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJoZWxsb0B4eXouY29tIiwiaWF0IjoxNTk2MjkxNzc3LCJleHAiOjE1OTYyOTIwNzd9.SlVpeMREQzUs0U-nqY4u86BVQU3JDvcTUjaZP4V_KFY
```

Sample Response:

```sh
 [
     {
         "id": 1,
         "countryName": "Indonesia",
         "gmtOffset": "GMT+7",
         "createdAt": "2020-08-01T14:22:38.000Z",
         "updatedAt": "2020-08-01T14:22:38.000Z"
     },
     {
         "id": 2,
         "countryName": "Malaysia",
         "gmtOffset": "GMT+8",
         "createdAt": "2020-08-01T14:22:38.000Z",
         "updatedAt": "2020-08-01T14:22:38.000Z"
     },
     {
         "id": 3,
         "countryName": "Singapore",
         "gmtOffset": "GMT+8",
         "createdAt": "2020-08-01T14:22:38.000Z",
         "updatedAt": "2020-08-01T14:22:38.000Z"
     }
 ]
```

**3. GET /country?name=xxx**  
Usage: This API is to return a country detail that is specified in the query string name. For this request, user needs to be authenticated first before he can call this API. To call this API, please pass in token that was obtained from POST /login in the request header _x-auth_  
Sample Request:

```sh
 In the request header:
 x-auth: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJoZWxsb0B4eXouY29tIiwiaWF0IjoxNTk2MjkxNzc3LCJleHAiOjE1OTYyOTIwNzd9.SlVpeMREQzUs0U-nqY4u86BVQU3JDvcTUjaZP4V_KFY

 URL:
 GET /country?name=indonesia
```

Sample Response:

```sh
 {
     "id": 1,
     "countryName": "Indonesia",
     "gmtOffset": "GMT+7",
     "createdAt": "2020-08-01T14:22:38.000Z",
     "updatedAt": "2020-08-01T14:22:38.000Z"
 }
```

Please take note that the country name that was passed in the query string is **case insensitive**

**4. POST /token**  
Usage: This API is to refresh the token. By default, token will expire in 5 minutes and refreshToken will expire in 24 hours  
Sample Request:

```sh
 {
     "email": "hello@xyz.com",
     "refreshToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJoZWxsb0B4eXouY29tIiwiaWF0IjoxNTk2MjgxNDU1LCJleHAiOjE1OTYzNjc4NTV9.xFWt21h6L3qb_LRu35tQnPreu0f8bBYrRzRvoEGyVww"
 }
```

Sample Response:

```sh
 {
     "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJoZWxsb0B4eXouY29tIiwiaWF0IjoxNTk2MjgxNDcwLCJleHAiOjE1OTYyODE3NzB9.MExiKVhSYkgEPwpsK0LOD7-7FGeVyJZIXtdHOl5mSlE"
 }
```
