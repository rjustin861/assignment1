const { UserModel } = require('../models/user_model');
const jwt = require('jsonwebtoken');

const env = process.env.NODE_ENV;
const config = require('../config')[env];

const { tokenSecret } = config.auth;

const Authenticate = async (req, res, next) => {
  let token = req.header('x-auth');
  if (!token) {
    return res.status(401).send();
  }

  try {
    decoded = await jwt.verify(token, tokenSecret);
    const user = UserModel.findOne({
      where: {
        id: decoded._id
      }
    });
    if (!user) {
      console.log('Invalid Token');
      return res.status(400).send({ Error: 'Invalid Token' });
    }
    req.user = user.id;
    req.token = token;
    next();
  } catch (error) {
    console.log(error);
    return res.status(400).send({ Error: 'Invalid Token' });
  }
};

module.exports = { Authenticate };
