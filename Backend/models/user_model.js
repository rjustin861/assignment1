const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');

const hashPassword = async (password) => {
  const saltRounds = 5;
  const hashed = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    });
  });

  return hashed;
};

const UserModel = sequelize.define('User', {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    autoIncrement: true,
    primaryKey: true
  },
  email: {
    type: Sequelize.STRING(50),
    allowNull: false,
    unique: true
  },
  password: {
    type: Sequelize.STRING(100),
    allowNull: false
  }
});

UserModel.drop();

UserModel.sync({}).then(async () => {
  //POPULATE Seed Data
  const password1 = await hashPassword('password1');
  const password2 = await hashPassword('password2');

  const userData = [
    {
      email: 'hello@xyz.com',
      password: password1
    },
    {
      email: 'test@xyz.com',
      password: password2
    }
  ];

  for (let user of userData) {
    try {
      await UserModel.create(user);
    } catch (error) {
      console.log('Error', error);
    }
  }
});

module.exports = { UserModel };
