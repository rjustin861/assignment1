const Sequelize = require('sequelize');

const CountryModel = sequelize.define('CountryDetail', {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    autoIncrement: true,
    primaryKey: true
  },
  countryName: {
    type: Sequelize.STRING(50),
    allowNull: false,
    unique: true
  },
  gmtOffset: {
    type: Sequelize.STRING(10),
    allowNull: false
  }
});

CountryModel.drop();

CountryModel.sync({}).then(async () => {
  //POPULATE Seed Data
  const countryData = [
    {
      countryName: 'Indonesia',
      gmtOffset: 'GMT+7'
    },
    {
      countryName: 'Malaysia',
      gmtOffset: 'GMT+8'
    },
    {
      countryName: 'Singapore',
      gmtOffset: 'GMT+8'
    }
  ];

  for (let country of countryData) {
    try {
      await CountryModel.create(country);
    } catch (error) {
      console.log('Error', error);
    }
  }
});

module.exports = { CountryModel };
