const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { UserModel } = require('../models/user_model');

const env = process.env.NODE_ENV;
const config = require('../config')[env];

const refreshTokens = {};

const errorMessage = {
  Error: 'Username or password is incorrect'
};

const {
  tokenSecret,
  tokenExpiry,
  refreshTokenSecret,
  refreshTokenExpiry
} = config.auth;

const compareHash = (string, hash) => {
  return new Promise((resolve, error) => {
    bcrypt.compare(string, hash, (err, success) => {
      if (err) {
        return error(err);
      }
      resolve(success);
    });
  });
};

const generateAuthToken = (email) => {
  const token = jwt.sign({ _id: email }, tokenSecret, {
    expiresIn: tokenExpiry
  });

  const refreshToken = jwt.sign({ _id: email }, refreshTokenSecret, {
    expiresIn: refreshTokenExpiry
  });

  refreshTokens[refreshToken] = email;

  return { token, refreshToken };
};

const UserService = {
  async login(email, password) {
    try {
      const user = await UserModel.findOne({
        where: { email }
      });

      //User not found
      if (!user) {
        console.log('User not found!');
        return errorMessage;
      }

      const match = await compareHash(password, user.password);
      if (!match) {
        //Password is incorrect
        console.log('Password is incorrect!');
        return errorMessage;
      }

      const token = generateAuthToken(email);
      return token;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  async refreshToken(email, refreshToken) {
    if (refreshToken in refreshTokens && refreshTokens[refreshToken] == email) {
      const token = jwt.sign({ _id: email }, tokenSecret, {
        expiresIn: tokenExpiry
      });

      // update the token in the list
      refreshTokens[refreshToken].token = token;
      return { token };
    } else {
      console.log('Email not found or refresh token not exists');
      return { Error: 'Invalid Email or Refresh Token' };
    }
  }
};

module.exports = { UserService };
