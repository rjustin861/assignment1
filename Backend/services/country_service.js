const Sequelize = require('sequelize');
const { CountryModel } = require('../models/country_model');

const CountryService = {
  async findAll() {
    try {
      const countries = await CountryModel.findAll();
      return countries;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  async findByName(name) {
    try {
      const country = await CountryModel.findOne({
        where: sequelize.where(
          //TAKE CARE case senstitive
          sequelize.fn('lower', sequelize.col('countryName')),
          sequelize.fn('lower', name)
        )
      });
      if (!country) return { Error: 'Country does not exists!' };
      return country;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
};

module.exports = { CountryService };
