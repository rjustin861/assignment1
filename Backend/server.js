const express = require('express');
const cors = require('cors');
const PORT = process.env.PORT || 3000;

//DB Connection and Models
require('./db/db_connect');
const { UserModel } = require('./models/user_model');
const { CountryModel } = require('./models/country_model');

//Express
const app = express();
app.use(express.json());
app.use(cors());

//Routes
require('./routes/user_route')(app);
require('./routes/country_route')(app);

app.listen(PORT, () => {
  console.log(`Server is running and listening at port ${PORT}`);
});
