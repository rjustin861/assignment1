const config = {
  development: {
    database: {
      host: process.env.DB_HOST,
      db: process.env.DB_NAME,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    auth: {
      tokenSecret: 'mytokensecret',
      tokenExpiry: 5 * 60,
      refreshTokenSecret: 'myrefreshtokensecret',
      refreshTokenExpiry: 24 * 60 * 60
    }
  }
};

module.exports = config;
