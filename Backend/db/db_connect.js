const Sequelize = require('sequelize');

const env = process.env.NODE_ENV;
const config = require('../config')[env];

const { db, username, password, host } = config.database;

const sequelize = new Sequelize(db, username, password, {
  host: host,
  dialect: 'mysql',
  operatorsAliases: '0'
});

const connectToDB = (async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection to DB has been established successfully');
  } catch (error) {
    console.log('Unable to connect to the database:', error);
  }
})();

global.sequelize = sequelize;
module.exports = sequelize;
