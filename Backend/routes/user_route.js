const { UserService } = require('../services/user_service');

module.exports = (app) => {
  app.get('/', (req, res) => {
    res.send('Hello World');
  });

  app.post('/login', async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
      console.log('Email and password are required');
      return res.status(400).send('Email and password are required');
    }

    try {
      const response = await UserService.login(email, password);
      res.send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send('Bad Request');
    }
  });

  app.post('/token', async (req, res) => {
    const { email, refreshToken } = req.body;
    if (!email || !refreshToken) {
      console.log('Email and refreshToken are required');
      return res.status(400).send('Email and refreshToken are required');
    }

    try {
      const response = await UserService.refreshToken(email, refreshToken);
      res.send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send('Bad Request');
    }
  });
};
