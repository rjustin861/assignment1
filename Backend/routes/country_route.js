const { CountryService } = require('../services/country_service');
const { Authenticate } = require('../middleware/authenticate');

module.exports = (app) => {
  app.get('/country', Authenticate, async (req, res) => {
    const name = req.query.name;
    let response;
    try {
      if (name) {
        response = await CountryService.findByName(name);
      } else {
        response = await CountryService.findAll();
      }
      res.send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send('Bad Request');
    }
  });
};
